(function () {
    'use strict';

    function ContactsListController($scope, contacts) {
        // get contacts from ContactsService
        $scope.contacts = contacts.query();

        // watch for changes of the model and update contacts model accordingly
        $scope.$watch(
            function () {
                return $scope.search;
            },
            function () {
                $scope.contacts = contacts.query({ q: $scope.search || null });
            });

        $scope.addContact = function addContact() {
            window.location.hash = '#/contact/new';
        };
    }

    angular.module('tactsApp.contactsList', ['ContactsService'])
    .controller('ContactsList', ['$scope', 'Contacts', ContactsListController]);

}());
