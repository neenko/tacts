(function () {
    'use strict';

    var emptyContact = {
        firstName: '',
        lastName: '',
        address: '',
        bookmarked: false,
        emails: [],
        phones: [],
        tags: []
    };

    function setTitle($scope, contact) {
        if (!contact.firstName && !contact.lastName) {
            $scope.title = '(unnamed)';
        } else {
            $scope.title = [contact.firstName, contact.lastName].join(' ');
        }
    }

    function saveContact(contacts, $scope, $location) {
        if ($scope.contact._id) {
            // perform an update if an id already present
            window.x = contacts.update({ contactId: $scope.contact._id }, $scope.contact);
        } else {
            // create a new contact
            window.x = contacts.save($scope.contact, function (contactData) {
                $scope.contact._id = contactData._id;
                $location.path('/contact/' + contactData._id).replace();
            });
        }
    }

    function deleteContact(contacts, id) {
        if (confirm('Are you sure?')) {
            contacts.remove({ contactId: id });
            window.location.href = '/';
        }
    }

    function ContactController($scope, $routeParams, $location, contacts) {
        if ($routeParams.contactId === 'new') {
            // if the route says 'new', set contact to a copy of an empty contact
            $scope.contact = angular.copy(emptyContact);
            // set title
            $scope.title = 'New contact';
        } else {
            // otherwise get contact details from the contacst service
            $scope.contact = contacts.get({ contactId: $routeParams.contactId });
            // once the promise resolves, set title
            $scope.contact.$promise.then(setTitle.bind(this, $scope));
        }

        $scope.save = function () {
            saveContact(contacts, $scope, $location);
        };

        $scope.remove = function () {
            deleteContact(contacts, $scope.contact._id);
        };
    }

    angular.module('tactsApp.contact', ['ContactsService'])
    .controller('Contact', ['$scope', '$routeParams', '$location', 'Contacts',  ContactController]);

}());
