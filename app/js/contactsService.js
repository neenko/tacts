(function () {
    'use strict';

    function ContactsServiceFactory($resource) {
        return $resource('rest/contacts/:contactId', {}, {
                query: { method: 'GET', isArray: true, params: { q: '@q' }},
                update: { method: 'PUT' }
            });
    }

    angular.module('ContactsService', ['ngResource'])
    .factory('Contacts', ['$resource', ContactsServiceFactory]);

}());
