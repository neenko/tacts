(function () {
    'use strict';

    angular.module('tactsApp', [
        'ngRoute',
        'ngTagsInput',
        'inputListDirective',
        'tactsApp.contactsList',
        'tactsApp.contact'
    ])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
        .when('/', {
            templateUrl: 'templates/contactsList.html',
            controller: 'ContactsList'
        })
        .when('/contact/:contactId', {
            templateUrl: 'templates/contact.html',
            controller: 'Contact'
        })
        .otherwise({
            redirectTo: '/'
        });
    }]);

}());
