(function () {
    'use strict';

    function InputList() {
        function link(scope, element) {
            // save a reference to the vew value input element
            scope.newInput = element.find('input');

            scope.add = function addItem() {
                var val = this.newInput.val();

                if (val != null) {
                    this.collection.push({ value: val });
                    this.newInput.val(null);
                }
            };

            scope.remove = function removeItem(childScope) {
                var parentScope = childScope.$parent,
                    index = childScope.$index;

                parentScope.collection.splice(index, 1);
            };
        }

        return {
                restrict: 'E',
                link: link,
                scope: { collection: '=' },
                templateUrl: 'directives/templates/inputList.html'
            };
    }

    angular.module('inputListDirective', [])
    .directive('inputList', InputList);
}());
