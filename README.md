# 'Tacts #
## A simple web contacts app ##

### Prerequisites ###
You'll need [NodeJS](https://nodejs.org) and [MongoDB](https://www.mongodb.org/).

### Installation ###
Clone this repo and run ```npm install```.

```
#!bash

git clone https://neenko@bitbucket.org/neenko/tacts.git
cd tacts
npm install
```

### Run the app ###
With MongoDB running, start the server.

```
#!bash

npm start
```
Open http://localhost:8080 in your browser.

### Notes ###
So far, nothing is configurable. The server only runs on port 8080 and MongoDB is expected to use its default port.