(function () {
    'use strict';

    var express = require('express'),
        bodyParser = require('body-parser'),
        mongoose = require('mongoose'),
        routes = require('./rest/routes'),
        app = express();

    app.use(bodyParser.json());

    // serve the 'tacts web app statically
    app.use(express.static('app'));

    // serve REST usins routes set in rest/routes.js
    app.use('/rest/contacts', routes);

    // fire up mongo connection
    mongoose.connect('mongodb://localhost/tacts', function (err) {
        if (err) {
            console.error('Error connecting to MongoDB/tacts.', err);
            process.exit(1);
        }
        console.log('Connected to mongodb://localhost/tacts');
        console.log('Listening on 8080...');
        app.listen(8080);
    });

}());
