(function () {
    'use strict';

    var express = require('express'),
        router = express.Router(),
        contact = require('./models/contact');

    function respond(res, next, err, contacts) {
        if (err) {
            return next(err);
        }
        res.json(contacts);
    }

    // contacts list, accepts 'q' param in the request for filtering
    router.get('/', function (req, res, next) {
        var query = req.query && req.query.q,
            contactsQuery;

        if (query) {
            contactsQuery = contact.findByQuery(query);
        } else {
            contactsQuery = contact.find({});
        }

        contactsQuery
        .sort({ bookmarked: 'desc', lastName: 'asc', firstName: 'asc' })
        .exec(respond.bind(this, res, next));
    });

    // get single contact details
    router.get('/:id', function (req, res, next) {
        contact.findById(req.params.id, respond.bind(this, res, next));
    });

    // create new contact
    router.post('/', function (req, res, next) {
        contact.create(req.body, respond.bind(this, res, next));
    });

    // update existing contact
    router.put('/:id', function (req, res, next) {
        contact.findByIdAndUpdate(req.params.id, req.body, respond.bind(this, res, next));
    });

    // delete contact
    router.delete('/:id', function (req, res, next) {
        contact.findByIdAndRemove(req.params.id, req.body, respond.bind(this, res, next));
    });

    module.exports = router;
}());
