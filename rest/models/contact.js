(function () {
    'use strict';

    var mongoose = require('mongoose'),
        escapeRx = require('escape-regexp'),

        // define mongo document schema for a contact
        contactSchema = new mongoose.Schema({
            firstName: { type: String, 'default': null },
            lastName: { type: String, 'default': null },
            address: { type: String, 'default': null },
            bookmarked: { type: Boolean, 'default': false },
            emails: { type : Array, 'default' : [] },
            phones: { type : Array, 'default' : [] },
            tags: { type : Array, 'default' : [] }
        });

    // add a method which finds contacts filtered by first/last name and tags
    contactSchema.static('findByQuery', function filterContacts(query) {
        var escaped = escapeRx(query),
            nameRegex = new RegExp(escaped, 'i');  // contains query, case insensitive

        return this.find({}).where({ $or: [
                // match first and last name by regex
                { firstName: nameRegex }, { lastName: nameRegex },
                // match a tag with exact query value
                { tags: { text: query }}
            ] });
    });

    module.exports = mongoose.model('contact', contactSchema);

}());
